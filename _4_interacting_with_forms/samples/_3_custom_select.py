from time import sleep

import pytest
from selenium.webdriver.common.by import By

from common import helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get("https://qalabs.pl/demos/web-samples/sample4.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver):
    # Get all selects list

    selects = driver.find_elements(By.CSS_SELECTOR, "div.md-select")

    # Handle single selection

    selects[0].click()

    menu_items = driver.find_elements(By.CSS_SELECTOR, ".md-menu-content-container li")
    menu_items[1].click()

    sleep(1)

    # Multiple selection

    selects[1].click()

    menu_items = driver.find_elements(By.CLASS_NAME, "md-list-item")
    menu_items[0].click()
    menu_items[2].click()

    # Sleep for a moment to show the dialog
    sleep(1)
