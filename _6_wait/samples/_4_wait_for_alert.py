from time import sleep

import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from common import helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver():
    driver = helpers.new_driver()
    driver.get("https://qalabs.pl/demos/web-samples/fizzbuzz.html")
    yield driver
    driver.close()


def test_waits_for_alert(driver):
    driver.find_element(By.ID, "number").send_keys("15")
    driver.find_element(By.ID, "submit").click()

    wait = WebDriverWait(driver, 5)
    wait.until(ec.alert_is_present())

    alert = driver.switch_to.alert
    assert alert.text == "FizzBuzz"
    sleep(1)
    alert.accept()
