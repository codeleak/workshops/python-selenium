import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver():
    driver = helpers.new_driver()
    driver.get("https://qalabs.pl/demos/web-samples/sample6.html")
    yield driver
    driver.close()


def test_verifies_button_states(driver):
    wait = WebDriverWait(driver, 30)

    button = driver.find_element(By.XPATH, "//*[contains(text(), 'Load')]")
    button.click()

    assert wait.until(lambda _: driver.find_element(By.XPATH, "//*[contains(text(), 'Warming up')]"))
    assert wait.until(lambda _: driver.find_element(By.XPATH, "//*[contains(text(), 'Loading')]"))
    assert wait.until(lambda _: driver.find_element(By.CSS_SELECTOR, ".md-progress-bar .md-progress-bar-fill").get_attribute("style") == "width: 100%;")
    assert wait.until(lambda _: driver.find_element(By.XPATH, "//*[contains(text(), 'Load')]").is_enabled())

    driver.save_screenshot(config.SCREENSHOTS_DIR + "/progress-bar.png")
