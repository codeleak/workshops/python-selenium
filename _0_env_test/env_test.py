from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions

from common import config


def test_env():
    print("Project root: " + config.PROJECT_ROOT)


def test_firefox():
    firefox_options = FirefoxOptions()
    firefox_options.set_preference("app.update.auto", False)
    firefox_options.set_preference("app.update.enabled", False)
    driver = webdriver.Firefox()

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_URL + "/sample1.html")

    # Assert page title
    assert "Sample 1" == driver.title

    # Save screenshot
    driver.save_screenshot(config.SCREENSHOTS_DIR + "/test_firefox.png")

    # Close the driver
    driver.close()


def test_chrome():
    chrome_options = ChromeOptions()
    chrome_options.accept_insecure_certs = True
    # chrome_options.add_argument(f"--user-data-dir={config.TMP_DIR}/chrome-data-dir")

    driver = webdriver.Chrome(options=chrome_options)

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_URL + "/sample1.html")

    # Assert page title
    assert "Sample 1" == driver.title

    # Save screenshot
    driver.save_screenshot(config.SCREENSHOTS_DIR + "/test_chrome.png")

    # Close the driver
    driver.close()
