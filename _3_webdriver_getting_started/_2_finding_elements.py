import pytest
from selenium.webdriver.common.by import By
from common import helpers


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get("https://qalabs.pl/demos/web-samples/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_finds_button_by_title(driver):
    button = driver.find_element(By.CSS_SELECTOR, "button[disabled='disabled'][type='button']")
    attr_value = button.get_attribute("data-custom-attribute")

    # Assertions. No changes required. Make sure all assertions pass
    assert not button.is_enabled()
    assert button.text == "DISABLED"
    assert "My custom attribute!" == attr_value
    assert button.is_displayed()


def test_verifies_3_subheadings_on_the_page(driver):
    divs = driver.find_elements(By.CSS_SELECTOR, "#paragraphs h1.md-subheading")

    # Assertions. No changes required. Make sure all assertions pass
    assert len(divs) == 3


def test_verifies_ordered_list_items(driver):
    list_items = driver.find_elements(By.CSS_SELECTOR, "#ordered-list li")
    list_items_texts = []
    for item in list_items:
        list_items_texts.append(item.text)

    # Assertions. No changes required. Make sure all assertions pass
    assert "List Item 1" in list_items_texts
    assert "List Item 2" in list_items_texts
    assert "List Item 3" in list_items_texts


def test_verifies_links_texts(driver):
    div = driver.find_element(By.ID, "links")
    links = div.find_elements(By.TAG_NAME, "a")
    link_texts = []
    for link in links:
        link_texts.append(link.text)

    # Assertions. No changes required. Make sure all assertions pass
    assert "Here we go!" in link_texts
    assert "Link 1 (clicked: false)" in link_texts
    assert "Link 2 (clicked: false)" in link_texts
    assert "Link 3 (clicked: false)" in link_texts


def test_verifies_names_in_the_table(driver):
    name_cells = driver.find_elements(By.CSS_SELECTOR, "tbody td:nth-of-type(2)")
    names = []
    for cell in name_cells:
        names.append(cell.text)

    # Assertions. No changes required. Make sure all assertions pass
    assert "Shawna Dubbin" in names
    assert "Odette Demageard" in names
    assert "Vera Taleworth" in names
